# Contents

This repository contains:

- The R-scripts `functions.R` and `model_fitting.R`
- A directory `data/`

The two `R` scripts are used to fit the models shown in my master's thesis. The script `model_fitting.R` sources the `functions.R` script and the fits the models, using the data that is stored in the `data/` directory. The models are stored within this directory as well.


Felix Hofmann, 2022